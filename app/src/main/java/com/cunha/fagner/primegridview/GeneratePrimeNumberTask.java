package com.cunha.fagner.primegridview;

import android.os.AsyncTask;

import java.util.ArrayList;

/**
 * Created by fagner on 08/01/17.
 */

public class GeneratePrimeNumberTask extends AsyncTask<Void, Integer, Void>{

    PrimeNumberListener mListener;
    ArrayList<Integer> primes;

    GeneratePrimeNumberTask(PrimeNumberListener listener){
        mListener = listener;
        primes = new ArrayList<>();
    }

    @Override
    protected Void doInBackground(Void... params){
        addPrime(2);
        addPrime(3);
        addPrime(5);
        addPrime(7);
        addPrime(11);

        int candidate = 13;

        //stop generating primes if occurs an overflow
        while (candidate > 0) {
            while (true) {
                boolean isPrime = true;

                for (Integer prime : primes) {
                    if (candidate % prime == 0) {
                        isPrime = false;
                        break;
                    }
                }

                if (isPrime) {
                    break;
                } else {
                    candidate = candidate + 2;
                }
            }
            addPrime(candidate);
            candidate = candidate + 2;
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mListener.onNewPrimeGenerated(values[0]);
    }

    private void addPrime(Integer prime){
        primes.add(prime);
        publishProgress(prime);
    }

    public interface PrimeNumberListener {
        void onNewPrimeGenerated(Integer prime);
    }
}
