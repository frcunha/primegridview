package com.cunha.fagner.primegridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private static final int NUMBER_COLUMNS = 4;

    private RecyclerView mRecyclerView;
    private PrimeAdapter mPrimeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_root);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, NUMBER_COLUMNS);
        mRecyclerView.setLayoutManager(layoutManager);

        mPrimeAdapter = new PrimeAdapter();
        mRecyclerView.setAdapter(mPrimeAdapter);
    }
}
