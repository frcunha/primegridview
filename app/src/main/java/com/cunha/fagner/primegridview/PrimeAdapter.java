package com.cunha.fagner.primegridview;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by fagner on 08/01/17.
 */

public class PrimeAdapter extends RecyclerView.Adapter<PrimeAdapter.PrimeViewHolder> implements GeneratePrimeNumberTask.PrimeNumberListener {

    private static final int MAX_NUMBER_ITEMS = 32767;
    private ArrayList<Integer> primeArray;
    private Random randomGenerator;

    PrimeAdapter(){
        primeArray = new ArrayList<>();
        randomGenerator = new Random(System.currentTimeMillis());
        new GeneratePrimeNumberTask(this).execute();
    }

    @Override
    public PrimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        PrimeViewHolder holder = new PrimeViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(PrimeViewHolder holder, int position) {
        int rand = randomGenerator.nextInt(primeArray.size());
        holder.tvNumber.setText(String.valueOf(primeArray.get(rand)));
    }

    @Override
    public int getItemCount() {
        return MAX_NUMBER_ITEMS;
    }

    @Override
    public void onNewPrimeGenerated(Integer prime) {
        primeArray.add(prime);
    }

    public static class PrimeViewHolder extends RecyclerView.ViewHolder{

        TextView tvNumber;

        public PrimeViewHolder(View itemView) {
            super(itemView);
            tvNumber = (TextView) itemView.findViewById(R.id.tv_number);
        }
    }
}
